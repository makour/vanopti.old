import React, { Component } from "react";
import { StyleSheet, View, Text, TextInput } from "react-native";
import MapView from "react-native-maps";
import LocationInput from "../components/LocationInput";
import Bouton from "../components/Bouton";
import Trajet from "../components/Trajet";
import GpsButton from "../components/GpsButton";

function MapScreen(props) {
  return (
    <View style={styles.container}>
      <MapView style={styles.mapView}
        provider={MapView.PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 36.754538953263314,
          longitude: 3.009979674483274,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        customMapStyle={[]}
      ></MapView>
      <View style={styles.rect}>
        <GpsButton />
        <Trajet />
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mapView: {
    flex: 1.5
  },
  rect: {
    flex: 1,
    // flexDirection: 'column',
    // justifyContent: 'space-evenly',
    // borderTopRightRadius: 20,
    // borderTopLeftRadius: 20,
    marginTop: -70,
    // backgroundColor: "white",
    // padding: 10,
  },
});

export default MapScreen;
