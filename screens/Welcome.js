import React, { Component } from "react";
import { StyleSheet, ImageBackground, Text, View } from "react-native";
import Bouton from "../components/Bouton";


function Welcome({ navigation }) {

    const goToLogin = () => {
        navigation.navigate('Login');
    }

    return (
        <ImageBackground source={require('../assets/images/bg.png')} style={styles.container}>
            <View></View>
            <View>
                <Text style={styles.textBig}>VANOPTI</Text>
                <Text style={styles.textSimple}>Trajets moins chers, tarifs plus justes, la mobilité comme on l'aime</Text>
            </View>
            <Bouton caption="C'est parti!" style={styles.bouton} onPress={goToLogin} />
        </ImageBackground>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        flexDirection: "column",
        justifyContent: "space-between",
        width: null,
        height: null,
    },
    container1: {
        alignSelf: "center",
    },
    bouton: {
        backgroundColor: "#38a3a5",
        flexDirection: "column",
    },
    textBig: {
        alignSelf: "center",
        color: "#fff",
        fontSize: 30,
        fontWeight: "bold",
    },
    textSimple: {
        textAlign: "center",
        color: "#fff",
        fontSize: 16,
        paddingVertical: 20,
        fontWeight: "700"
    },
});

export default Welcome;
