import React, { Component } from "react";
import { StyleSheet, Text, View, Linking } from "react-native";
import Bouton from "../components/Bouton";
import Phone from "../components/Phone";




function Login(props) {

    return (
        <View style={styles.container}>
            <Text style={styles.textBig}>VANOPTI</Text>
            <Text style={styles.textSimple}>Utilisez votre numéro de téléphone pour vous connecter.</Text>
            <Phone></Phone>
            <Bouton style={styles.bouton} caption="Envoyer" onPress={() => props.navigation.navigate('Otp')}></Bouton>
            <Text style={styles.textP}>En vous connectant à VANOPTI vous acceptez</Text>
            <Text style={styles.textP2} onPress={() => Linking.openURL('https://www.cmconsulting-dz.com')} >Les conditions générales d'utilisations.</Text>
        </View>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        flexDirection: "column",
        justifyContent: "center",
        width: null,
        height: null,

    },
    bouton: {
        backgroundColor: "#38a3a5",
        flexDirection: "column",
        paddingVertical: 10
    },
    textBig: {
        alignSelf: "center",
        color: "#38A3A5",
        fontSize: 30,
        fontWeight: "bold",
    },
    textSimple: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 14,
        paddingVertical: 20,
        paddingHorizontal: 10,
        fontWeight: "500"
    },
    textP: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 12,
        paddingTop: 20,
        fontWeight: "300"
    },
    textP2: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 12,
        fontWeight: "bold"
    },
});

export default Login;
