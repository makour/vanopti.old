import React, { Component } from "react";
import { StyleSheet, Text, View, Linking, TextInput } from "react-native";
import Bouton from "../components/Bouton";
import OTPInputView from '@twotalltotems/react-native-otp-input'




function Otp(props) {


    return (
        <View style={styles.container}>
            <Text style={styles.textBig}>VANOPTI</Text>
            <Text style={styles.textSimple}>Utilisez votre numéro de téléphone pour vous connecter.</Text>
            <OTPInputView
                style={styles.containerOtp}
                pinCount={6}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                // onCodeChanged = {code => { this.setState({code})}}
                autoFocusOnLoad
                codeInputFieldStyle={styles.InputOtp}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
            // onCodeFilled={(code => {console.log(`Code is ${code}, you are good to go!`)})}
            />
            <Bouton style={styles.bouton} caption="Envoyer" onPress={() => props.navigation.navigate('Welcome')}></Bouton>
            <Text style={styles.textP}>En vous connectant à VANOPTI vous acceptez</Text>
            <Text style={styles.textP2} onPress={() => Linking.openURL('https://www.cmconsulting-dz.com')} >Les conditions générales d'utilisations.</Text>
        </View>
    );
}

var styles = StyleSheet.create({
    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },
    container: {
        flex: 1,
        padding: 30,
        flexDirection: "column",
        justifyContent: "center",
        width: null,
        height: null,
    },
    containerOtp: {
        flexDirection: "row",
        justifyContent: "center",
        paddingVertical: 20,
    },
    bouton: {
        backgroundColor: "#38a3a5",
        flexDirection: "column",
        paddingVertical: 10
    },
    InputOtp: {
        backgroundColor: "#f5f5f5",
        paddingHorizontal: 15,
        marginHorizontal: 3,
        borderRadius: 5,
        borderBottomWidth: 3,
        borderBottomColor: "#38A3A5",
        aspectRatio: 1 / 1,
        textAlign: "center",
        fontSize: 18,
        fontWeight: "700",
        color: "#343A40",
        borderWidth: 0
    },
    textBig: {
        alignSelf: "center",
        color: "#38A3A5",
        fontSize: 30,
        fontWeight: "bold",
    },
    textSimple: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 14,
        paddingVertical: 20,
        paddingHorizontal: 10,
        fontWeight: "500"
    },
    textP: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 12,
        paddingTop: 20,
        fontWeight: "300"
    },
    textP2: {
        textAlign: "center",
        color: "#343A40",
        fontSize: 12,
        fontWeight: "bold"
    },
});

export default Otp;
