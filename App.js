// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import Welcome from './screens/Welcome';
// import Login from './screens/Login';
// import Otp from './screens/Otp';
// import MapScreen from './screens/MapScreen';
// import Dimensions from './screens/Dimensions';

// const Stack = createNativeStackNavigator();

// export default function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         {/* <Stack.Screen name="Welcome" component={Welcome} options={{ headerShown: false }} />
//         <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
//         <Stack.Screen name="Otp" component={Otp} options={{ headerShown: false }} /> */}
//         {/* <Stack.Screen name="MapScreen" component={MapScreen} options={{ headerShown: false }} /> */}
//         <Stack.Screen name="Dimensions" component={Dimensions} options={{ headerShown: false }} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//     margin: 20
//   }
// });

import Expo from "expo";
import React, { Component } from "react";
import { GLView } from "expo-gl";
import ExpoTHREE, { Renderer } from "expo-three";
import * as THREE from "three";
import { Asset } from "expo-asset";
import { TextureLoader } from 'expo-three';
import { MeshStandardMaterial } from "three";
import OrbitControlsView from 'expo-three-orbit-controls';
import { StyleSheet, TextInput } from "react-native";
import { View, Text } from "react-native";
import Slider from '@react-native-community/slider';
import NumericInput from 'react-native-numeric-input'

export default class App extends Component {

  handleChange(event) { this.setState({ value: event.target.value }); }
  constructor(props) {
    super(props)
    this.state = {
      h: 0.5,
      minH: 0.01,
      maxH: 3,
      w: 0.5,
      minW: 0.01,
      maxW: 3,
      d: 0.5,
      minD: 0.01,
      maxD: 3,
    }
  }


  render() {
    return (
      <>
        <GLView style={styles.box} onContextCreate={this._onGLContextCreate} />
        <View style={styles.fields}>
          <NumericInput
            // initValue={this.state.h}
            onChange={this.handleChange}
            value={this.state.h}
            onChange={value => { this.handleChange; this.setState({ h: value }) }}
            // totalWidth={240} 
            // totalHeight={50} 
            minValue={this.state.minH}
            maxValue={this.state.maxH}
            iconSize={25}
            step={0.01}
            valueType='real'
            rounded
            textColor='white'
            iconStyle={{ color: 'white' }}
            rightButtonBackgroundColor='#38a3a5'
            leftButtonBackgroundColor='#38a3a5'
            borderColor='#38a3a5'
            totalHeight={30} />
          <TextInput style={styles.field}></TextInput>
          <TextInput style={styles.field}></TextInput>
        </View>
        <View style={styles.menu}>
          <View style={styles.container}>
            <Slider
              style={{ width: 300 }}
              step={0.01}
              minimumValue={this.state.minH}
              maximumValue={this.state.maxH}
              value={this.state.h}
              onValueChange={val => { this.setState({ h: val }); this.forceUpdate(); }}
              thumbTintColor='rgb(252, 228, 149)'
              maximumTrackTintColor='#d3d3d3'
              minimumTrackTintColor='rgb(252, 228, 149)'
            />
            <View style={styles.textCon}>
              <Text style={styles.colorGrey}>{this.state.minH} m</Text>
              <Text style={styles.colorYellow}>
                {this.state.h + 'm'}
              </Text>
              <Text style={styles.colorGrey}>{this.state.maxH} m</Text>
            </View>
          </View>
          <View style={styles.container}>
            <Slider
              style={{ width: 300 }}
              step={0.01}
              minimumValue={this.state.minD}
              maximumValue={this.state.maxD}
              value={this.state.d}
              onValueChange={val => this.setState({ d: val })}
              thumbTintColor='rgb(252, 228, 149)'
              maximumTrackTintColor='#d3d3d3'
              minimumTrackTintColor='rgb(252, 228, 149)'
            />
            <View style={styles.textCon}>
              <Text style={styles.colorGrey}>{this.state.minD} m</Text>
              <Text style={styles.colorYellow}>
                {this.state.h + 'm'}
              </Text>
              <Text style={styles.colorGrey}>{this.state.maxD} m</Text>
            </View>
          </View>
          <View style={styles.container}>
            <Slider
              style={{ width: 300 }}
              step={0.01}
              minimumValue={this.state.minW}
              maximumValue={this.state.maxW}
              value={this.state.w}
              onValueChange={val => this.setState({ w: val })}
              thumbTintColor='rgb(252, 228, 149)'
              maximumTrackTintColor='#d3d3d3'
              minimumTrackTintColor='rgb(252, 228, 149)'
            />
            <View style={styles.textCon}>
              <Text style={styles.colorGrey}>{this.state.minW} m</Text>
              <Text style={styles.colorYellow}>
                {this.state.w + 'm'}
              </Text>
              <Text style={styles.colorGrey}>{this.state.maxW} m</Text>
            </View>
          </View>
        </View>
      </>
    );
  }
  _onGLContextCreate = async (gl) => {
    // 1. Scene
    var scene = new THREE.Scene();
    // 2. Camera
    const camera = new THREE.PerspectiveCamera(
      75,
      gl.drawingBufferWidth / gl.drawingBufferHeight,
      0.1,
      1000
    );

    gl.canvas = { width: gl.drawingBufferWidth, height: gl.drawingBufferHeight };
    // 3. Renderer
    const renderer = new Renderer({ gl });
    renderer.setSize(gl.drawingBufferWidth, gl.drawingBufferHeight);
    const geometry = new THREE.BoxGeometry(1, 0.5, 1);
    const material = new THREE.MeshBasicMaterial({
      map: new TextureLoader().load(require('./assets/images/texture.jpeg')),
    });

    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
    let new_geometry;
    camera.position.z = 5;
    const animate = () => {
      requestAnimationFrame(animate);
      cube.rotation.x = 10;
      cube.rotation.y = 5.2;
      let new_geometry = new THREE.BoxGeometry(this.state.w, this.state.h, this.state.d);
      geometry.dispose();
      cube.geometry = new_geometry;
      renderer.render(scene, camera);
      gl.endFrameEXP();
    };
    animate();
  };
}

const styles = StyleSheet.create({
  box: {
    flex: 1,
    backgroundColor: '#aaa',
    width: "100%",
    height: "100%"
  },
  menu: {
    flex: 0.5,
    // backgroundColor: '#eee',
    // alignItems: 'center',
    // justifyContent: 'center',
    // margin: 20
  },
  slider: {
    flex: 1,
    // backgroundColor: '#eee',
    // alignItems: 'center',
    // justifyContent: 'center',
    // margin: 20
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  textCon: {
    width: 320,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  colorGrey: {
    color: '#d3d3d3'
  },
  colorYellow: {
    color: 'rgb(252, 228, 149)'
  },
  fields: {
    // flex: 0.1,
    height: 50,
    backgroundColor: '#777',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#343A40',
    padding: 10
  },
  field: {
    flex: 1,
    backgroundColor: "white",
    margin: 8,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#38a3a5",
    padding: 5,
  }
});

