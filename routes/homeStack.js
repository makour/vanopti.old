import { createStackNavigator } from 'react-navigation/stack';
import { createAppContainer } from 'react-navigation';
import Welcome from '../screens/Welcome';
import Login from '../screens/Login';
import Otp from '../screens/Otp';

const screens = {
    Welcome: {
        screen: Welcome,
    },
    Login: {
        screen: Login,
    },
    Otp: {
        screen: Otp,
    },
};

// home stack navigator screens
const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);