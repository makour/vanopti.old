import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text, TextInput, View } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';


function LocationInput(props) {

    return (
        <View style={styles.searchSection}>
            <Icon style={styles.searchIcon} name="map-marker" size={30} color="#000" />
            <TextInput
                style={styles.input}
                placeholder={props.placeholder}
            // underlineColorAndroid="#38a3a5"
            />
        </View>
    );
}

const styles = StyleSheet.create({
    searchSection: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#eee",
        margin: 10,
        // shadowColor: '#000',
        // shadowOffset: { width: 1, height: 1 },
        // shadowOpacity: 0.2,
        // shadowRadius: 10,
        // elevation: 5,
    },
    searchIcon: {
        paddingRight: 10,
        paddingVertical: 5,
        paddingLeft: 15,
        color: "#343A40",
        fontSize: 18,
    },
    input: {
        flex: 1,
        paddingHorizontal: 10,
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: '#fff',
        color: '#343A40',
        fontWeight: "500",
        fontSize: 16,
    },
});

export default LocationInput;
