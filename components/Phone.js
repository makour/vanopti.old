import React from 'react';
import { StyleSheet } from 'react-native';
import PhoneInput from 'react-native-phone-input'

export default function Phone(props) {
    return (
        <PhoneInput style={[props.style, styles.container]}
            onPressFlag={() => { }}
            initialCountry='dz'
            allowZeroAfterCountryCode={false}
            flagStyle={{ borderWidth: 2, borderRadius: 5, marginLeft: 10 }}
        // offset='10'
        />
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f5f5f5',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#343A40",
        paddingVertical: 5,
        marginVertical: 5

    },
});
