import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

function GpsButton(props) {

    return (
        <TouchableOpacity style={[styles.container, props.style]} onPress={props.onPress}>
            <Icon style={styles.icon} name="crosshairs" size={30} color="#000" />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#38a3a5",
        flexDirection: 'row',
        justifyContent: "center",
        alignSelf: "flex-end",
        aspectRatio: 1,
        borderRadius: 30,
        padding: 12,
        margin: 10,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.6,
        shadowRadius: 30,
        elevation: 5,
    },
    icon: {
        color: "#fff",
        fontSize: 26,
        fontWeight: "300",
        alignSelf: "center"
    }
});

export default GpsButton;
