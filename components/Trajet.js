import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import LocationInput from "../components/LocationInput";
import Bouton from "../components/Bouton";

function Trajet(props) {
    return (

        <View style={styles.rect}>
            <Text style={styles.ouAllezVous}>Où allez-vous ?</Text>
            <View style={styles.location}>
                <LocationInput style={styles.locationInput} placeholder={'Votre position'}></LocationInput>
                <LocationInput style={styles.locationInput} placeholder={'Choisir une destination'} ></LocationInput>
            </View>
            <Bouton style={styles.bouton} caption={'Suivant'} />
        </View>
    );
}

const styles = StyleSheet.create({
    rect: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        marginTop: -5,
        backgroundColor: "white",
        padding: 10,
    },
    ouAllezVous: {
        color: "#343A40",
        textAlign: "center",
        fontSize: 18,
        fontWeight: "700",
    },
    location: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        // paddingRight: 20
    },
    locationInput: {
    },
    mapViewStack: {
        flex: 1
    },
    bouton: {
        // marginTop: 50,
        backgroundColor: "#38a3a5",
        paddingVertical: 10,
        marginHorizontal: 10
    }
});

export default Trajet;
