import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";


function Bouton(props) {

    return (
        <TouchableOpacity style={[styles.container, props.style]} onPress={props.onPress}>
            <Text style={styles.caption}>{props.caption || "Button"}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#343A40",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 16,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 10,
        elevation: 5,
    },
    caption: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "700",
    }
});

export default Bouton;
